module.exports = {
  plugins: ['jsx-a11y', 'react'],
  "parserOptions": {
    "ecmaFeatures": {
      "jsx": true
    }
  },

  rules: {
    /* jsx rules */
    'jsx-a11y/anchor-has-content': 'off',
    'jsx-a11y/no-noninteractive-element-interactions': 'off',
    'jsx-a11y/no-static-element-interactions': 'off',
    "jsx-a11y/anchor-is-valid": [ "error", {
      "components": [ "Link" ],
      "specialLink": [ "hrefLeft", "hrefRight" ],
      "aspects": [ "noHref", "invalidHref" ],
    }],
    /* deprecated rules href-no-hash, replaced by jsx-a11y/anchor-is-valid */
    'jsx-a11y/href-no-hash': 'off',
  },
};

