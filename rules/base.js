module.exports = {
  plugins: ['import'],
  rules: {
    'arrow-body-style': 'off',
    'arrow-parens': ['error', 'as-needed'],
    'class-methods-use-this': 'off',
    'comma-dangle': ['error', 'always-multiline'], // https://github.com/airbnb/javascript/commit/788208295469e19b806c06e01095dc8ba1b6cdc9
    'func-names': 'error',
    indent: 'off',
    'newline-after-var': ['error', 'always'],
    'newline-before-return': 'error',
    'newline-per-chained-call': 'off',
    'no-alert': 0,
    'no-confusing-arrow': 'off',
    'no-console': 0,
    'no-constant-condition': 'error',
    'no-mixed-operators': [
      'error',
      {
        groups: [
          ['&', '|', '^', '~', '<<', '>>', '>>>'],
          ['==', '!=', '===', '!==', '>', '>=', '<', '<='],
          ['&&', '||'],
          ['in', 'instanceof'],
        ],
        allowSamePrecedence: true,
      },
    ],
    'no-restricted-syntax': ['error', 'DebuggerStatement', 'ForInStatement', 'WithStatement'],
    'no-restricted-properties': [
      'error',
      {
        object: 'arguments',
        property: 'callee',
        message: 'arguments.callee is deprecated',
      },
      {
        property: '__defineGetter__',
        message: 'Please use Object.defineProperty instead.',
      },
      {
        property: '__defineSetter__',
        message: 'Please use Object.defineProperty instead.',
      },
    ],
    'no-underscore-dangle': 'off',
    'max-len': 'off',
    /* import rules */
    'import/no-extraneous-dependencies': [
      'error',
      {
        devDependencies: [
          'test/**', // tape, common npm pattern
          'tests/**', // also common npm pattern
          'spec/**', // mocha, rspec-like pattern
          '**/__tests__/**', // jest pattern
          '**/__mocks__/**', // jest pattern
          '**/*.stories.js', // storybook pattern
          'stories/**/*.js', // storybook pattern
          'test.js', // repos with a single test file
          'test-*.js', // repos with multiple top-level test files
          'features/**', // cucumber
          '**/*.test.js', // tests where the extension denotes that it is a test
          '**/webpack.config.js', // webpack config
          '**/webpack.config.*.js', // webpack config
          '**/rollup.config.js', // rollup config
          '**/gulpfile.js', // gulp config
          '**/gulpfile.*.js', // gulp config
          '**/Gruntfile', // grunt config
          'config/jest/**',
          'jest/**',
          'e2e/**',
          'appium/**',
          'src/testUtils/**',
          '*.js',
        ],
        optionalDependencies: false,
      },
    ],
    'no-plusplus': ['error', { allowForLoopAfterthoughts: true }],
    'prefer-destructuring': 'off',
    'space-before-function-paren': ['error', 'never'],
    'wrap-iife': ['error', 'inside', {
      functionPrototypeMethods: false,
    }],
  },
};
